﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Interface;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data.Services
{
    public class StudentService : IStudentService
    {

        private readonly AppDBContent _appDBContent;

        public StudentService(AppDBContent appDBContent)
        {
            this._appDBContent = appDBContent;
        }

        public IEnumerable<Student> GetStudents => _appDBContent.Students.Include(p => p.Group);

        public Student GetStudentByID(int id) => (Student)_appDBContent.Students.FirstOrDefault(p => p.Id == id);
        

        public IEnumerable<Student> GetStudentsByGroupID(int groupID) => _appDBContent.Students.Where(p => p.GroupID == groupID).Include(p => p.Group) ;

        private bool CreateStudent(Student student)
        {
            if (student != null && student.FirstName != null && student.LastName != null)
            {
                _appDBContent.Students.Add(student);
                _appDBContent.SaveChanges();
                return true;
            }
            return false;
        }

        private bool UpdateStudent(Student student , Student dbStudent)
        {
            if (student.FirstName != null && student.LastName != null)
            {
                dbStudent.FirstName = student.FirstName;
                dbStudent.LastName = student.LastName;
                _appDBContent.Students.Update(dbStudent);
                _appDBContent.SaveChanges();
                return true;
            }
            return false;
        }

        public bool  Save(Student student)
        {
            Student dbStudent = (Student)_appDBContent.Students.FirstOrDefault(p => p.Id == student.Id);
            Group dbGroup = (Group)_appDBContent.Group.FirstOrDefault(p => p.Id == student.GroupID);
            if (dbStudent == null && dbGroup != null)
            {
                return CreateStudent(student);
            }
            else if(dbStudent !=null && dbGroup !=null )
            {
                return UpdateStudent(student, dbStudent);
            }
            return false;
        }

        public bool Delete(int id)
        {
            Student dbStudent = (Student)_appDBContent.Students.FirstOrDefault(p => p.Id == id);
            if (dbStudent != null)
            {
                _appDBContent.Students.Remove(dbStudent);
                _appDBContent.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
            
        }

        
    }
}
