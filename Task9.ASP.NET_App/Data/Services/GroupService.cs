﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Interface;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data.Services
{
    public class GroupService : IGroupService
    {

        private readonly AppDBContent _appDBContent;

        public GroupService(AppDBContent appDBContent)
        {
            this._appDBContent = appDBContent;
        }

        public IEnumerable<Group> GetAllGroups => _appDBContent.Group.Include(c =>c.Course);

        public IEnumerable<Group> GetGroupByCourseID(int courseID) => _appDBContent.Group.Where(p => p.CourseId == courseID).Include(p => p.Course);

        public Group GetGroupByID(int id) => (Group)_appDBContent.Group.FirstOrDefault(p => p.Id == id);

        private bool CreateGroupe(Group group)
        {
            if (group != null && group.Name != null)
            {
                _appDBContent.Group.Add(group);
                _appDBContent.SaveChanges();
                return true;

            }
            return false;
        }

        private bool UpdateGroup(Group group,Group dbGroup)
        {
            if (group.Name != null)
            {
                dbGroup.Name = group.Name;
                _appDBContent.Group.Update(dbGroup);
                _appDBContent.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Save(Group group)
        {
            Group dbGroup = _appDBContent.Group.FirstOrDefault(p => p.Id == group.Id);
            Course dbCourse = _appDBContent.Course.FirstOrDefault(p => p.Id == group.CourseId);

            if(dbGroup == null && dbCourse !=null)
            {
                return CreateGroupe(group);

            }
            else if(dbGroup!= null && dbCourse!=null)
            {
                return UpdateGroup(group, dbGroup);   
            }
            return false;
        }

        public bool Delete(int groupId)
        {
            Group dbGroup = _appDBContent.Group.FirstOrDefault(p => p.Id == groupId);
            Student dbStudent = (Student)_appDBContent.Students.FirstOrDefault(p => p.GroupID == groupId);
            if(dbGroup !=null && dbStudent == null)
            {
                _appDBContent.Group.Remove(dbGroup);
                _appDBContent.SaveChanges();
                return true;
            }
            return false;

        }

      
    }
}
