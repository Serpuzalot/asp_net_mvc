﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Interface;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data.Services

{
    public class CourseService : ICourseService
    {
        private readonly AppDBContent _appDBContent;

        public CourseService (AppDBContent appDBContent)
        {
            this._appDBContent = appDBContent;
        }

        public IEnumerable<Course> GetAllCourse => _appDBContent.Course;

        public Course GetCourseByID(int id) => (Course)_appDBContent.Course.FirstOrDefault(p => p.Id == id);

        private void CreateCourse(Course course)
        {
            _appDBContent.Add(course);
            _appDBContent.SaveChanges();
        }

        private void UpdateCourse(Course course,Course dbCourse)
        {
            dbCourse.Name = course.Name;
            dbCourse.Description = course.Description;
            _appDBContent.Course.Update(dbCourse);
            _appDBContent.SaveChanges();
        }

        public bool SaveEdit(Course course)
        {
            Course dbCourse = (Course)_appDBContent.Course.FirstOrDefault(p => p.Id == course.Id);
            if(dbCourse == null && course.Name != null && course.Description != null)
            {
                CreateCourse(course);
                return true;
            }
            else if(course.Name != null && course.Description != null && dbCourse!=null)
            {
                UpdateCourse(course, dbCourse);
                return true;
            }
            return false;
        }

        public bool DeleteCourse(int id)
        {
           
            Course dbCourse = (Course)_appDBContent.Course.FirstOrDefault(p => p.Id == id);
            Group dbGroup = (Group)_appDBContent.Group.FirstOrDefault(p => p.CourseId == id);
            if(dbGroup == null && dbCourse !=null)
            {
                _appDBContent.Course.Remove(dbCourse);
                _appDBContent.SaveChanges();
                return true;
            }
            return false;
            
        }

        public void AddCourse(Course course)
        {
            _appDBContent.Course.Add(course);
            _appDBContent.SaveChanges();
        }
        

    }
}
