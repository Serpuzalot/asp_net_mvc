﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data
{
    public class AppDBContent : DbContext
    { 

        public AppDBContent(DbContextOptions<AppDBContent> options ) : base(options)
        {
           
        } 

        public DbSet<Course> Course { get; set; }

        public DbSet<Group> Group { get; set; }

        public DbSet<Student> Students { get; set; }
    }
}
