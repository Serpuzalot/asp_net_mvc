﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data
{
    public class DBObjects
    {
        public static void Initial(AppDBContent content)
        {

            if (!content.Course.Any())
            {
                content.Course.AddRange(Courses.Select(c => c.Value));
            }

            if (!content.Group.Any())
            {
                content.Group.AddRange(Groups.Select(c => c.Value));
            }

            if (!content.Students.Any())
            {
                content.AddRange(
                    new Student { Group = Groups["AT-201"], FirstName="Omni",LastName="Knight"},
                    new Student { Group = Groups["AT-192"], FirstName = "Lucretsia", LastName = "Girl" },
                    new Student { Group = Groups["AT-181"], FirstName = "Grey", LastName = "Fullbaster" },
                    new Student { Group = Groups["AI-171"], FirstName = "Sub", LastName = "Zero" },
                    new Student { Group = Groups["AT-201"], FirstName = "Wok", LastName = "Wind" },
                    new Student { Group = Groups["AT-192"], FirstName = "Joe", LastName = "Full" },
                    new Student { Group = Groups["AT-181"], FirstName = "Georg", LastName = "Freak" },
                    new Student { Group = Groups["AI-171"], FirstName = "Scorpion", LastName = "Fire" }
                );
            }

            content.SaveChanges();
        }

        private static Dictionary<string, Course> course;
        public static Dictionary<string, Course> Courses
        {
            get
            {
                if(course == null)
                {
                    var list = new Course[]
                    {
                        new Course { Name = "First" , Description = "Nice course"},
                        new Course { Name = "Second" , Description = "Bad course"},
                        new Course { Name = "Third" , Description = "Interesting course"},
                        new Course { Name = "Fourth" , Description = "Fatality"}
                    };
                    course = new Dictionary<string, Course>();
                    foreach(Course el in list)
                    {
                        course.Add(el.Name, el);
                    }
                }
                return course;
            }
        }

        private static Dictionary<string, Group> groups;

        public static Dictionary<string, Group> Groups
        {
            get
            {
                if (groups == null)
                {
                    var list = new Group[]
                    {
                        new Group { Name = "AT-201", Course = Courses["First"] },
                        new Group { Name = "AT-192", Course = Courses["Second"] },
                        new Group { Name = "AT-181", Course = Courses["Third"] },
                        new Group { Name = "AI-171", Course = Courses["Fourth"]}
                    };
                    groups = new Dictionary<string, Group>();
                    foreach(Group el in list)
                    {
                        groups.Add(el.Name, el);
                    }
                }
                return groups;
            }
        }

    }
}
