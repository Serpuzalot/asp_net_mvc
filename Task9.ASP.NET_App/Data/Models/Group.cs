﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task9.ASP.NET_App.Data.Models
{
    public class Group
    {
        public int Id { get; set; }

        public string  Name { get; set; }

        public int CourseId { get; set; }

        public virtual Course Course { get; set; }

        public List<Student> StudentList { get; set; }
    }
}
