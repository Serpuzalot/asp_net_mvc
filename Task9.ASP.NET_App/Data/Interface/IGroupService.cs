﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data.Interface
{
    public interface IGroupService
    {
        IEnumerable<Group> GetAllGroups { get; }

        IEnumerable<Group> GetGroupByCourseID(int courseID);

        Group GetGroupByID(int id);

        public bool Save(Group group);

        public bool Delete(int groupId);


    }
}
