﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data.Interface
{
    public interface IStudentService
    {

        IEnumerable<Student> GetStudents { get; }

        IEnumerable<Student> GetStudentsByGroupID(int groupID);

        Student GetStudentByID(int id);

        public bool Save(Student student);

        public bool Delete(int id);
    }
}
