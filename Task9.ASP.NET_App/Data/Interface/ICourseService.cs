﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Models;

namespace Task9.ASP.NET_App.Data.Interface
{
    public interface ICourseService
    {
        public IEnumerable<Course> GetAllCourse { get; }

        Course GetCourseByID(int id);

        public bool SaveEdit(Course course);

        public bool DeleteCourse(int id);

        public void AddCourse(Course course);




    }
}
