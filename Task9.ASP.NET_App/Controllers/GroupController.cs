﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data.Interface;
using Task9.ASP.NET_App.Data.Models;
using Task9.ASP.NET_App.Data;
using Task9.ASP.NET_App.Data.Services;

namespace Task9.ASP.NET_App.Controllers
{
    public class GroupController : Controller
    {
        private readonly IGroupService _groupService;
        private readonly ICourseService _courseService;

        public GroupController(IGroupService groupService ,ICourseService courseService)
        {
            this._groupService = groupService;
            this._courseService = courseService;

        }


        public ViewResult CourseGroups(int id)
        {
            ViewBag.Title = "Groups";
            var cours = _courseService.GetCourseByID(id);
            if(cours != null)
            {
                ViewBag.CourseName = cours.Name;
                var groups = _groupService.GetGroupByCourseID(id);
                ViewBag.CourseId = id;
                if (groups == null)
                {
                    return View();
                }

                return View(groups);
            }
            return View();
        }
       
       public ViewResult Edit(int id)
        {
            var group = _groupService.GetGroupByID(id);
            return View(group);
        }

        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult Save(Group group)
        {
            bool result = _groupService.Save(group);
            if (result)
            {
                ViewBag.Message = "Successfully!";
            }
            else
            {
                ViewBag.Message = "Error ,try again!";
            }
            return View(group);
        }

        public ViewResult Delete(int id)
        {
            var group = _groupService.GetGroupByID(id);
            bool result = _groupService.Delete(id);
            if (result)
            {
                ViewBag.CourseId = group.CourseId;
                ViewBag.Message = "Successfully!";
            }
            else
            {
                ViewBag.Message = "Error,can not delete group with students,or group do not exist!";
            }
            return View();
        }

        public ViewResult AddGroup(int courseId)
        {
            ViewBag.CourseId = courseId;
            return View();
        }

    }
}
