﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data;
using Task9.ASP.NET_App.Data.Interface;
using Task9.ASP.NET_App.Data.Models;
using Task9.ASP.NET_App.Data.Services;

namespace Task9.ASP.NET_App.Controllers
{
    public class CourseController : Controller
    {

        private readonly ICourseService _courseService;


        public CourseController(ICourseService courseService)
        {
            this._courseService = courseService;

        }

        public ViewResult ShowCourse()
        {
            ViewBag.Tile = "Courses";
            var course = _courseService.GetAllCourse;
            return View(course);
        }

        public ViewResult EditCourse(int id)
        {
            ViewBag.Title = "Edit Course";
            var course = _courseService.GetCourseByID(id);
            if(course != null)
            {

                return View(course);
            }
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveEdit(Course course)
        {
            bool result = _courseService.SaveEdit(course);
            if (result)
            {
                ViewBag.Message = "Successfully!";
            }
            else
            {
                ViewBag.Message = "Error,try again!";
            }
            return View();
        }

        public ActionResult DeleteCourse(int id)
        {
            bool result = _courseService.DeleteCourse(id);
            if (result)
            {
                ViewBag.Message = "Successfully!";
            }
            else
            {
                ViewBag.Message = "Error,Can not delete course with group inside,or course do not exist!";
            }
            
            return View();
        }

 

        public ActionResult AddCourse()
        {
            return View();

        }

        


    }
}
