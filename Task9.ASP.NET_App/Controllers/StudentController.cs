﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task9.ASP.NET_App.Data;
using Task9.ASP.NET_App.Data.Interface;
using Task9.ASP.NET_App.Data.Models;
using Task9.ASP.NET_App.Data.Services;

namespace Task9.ASP.NET_App.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;
        private readonly IGroupService _groupService;

        public StudentController(IStudentService studentService,IGroupService groupService)
        {
            this._studentService = studentService;
            this._groupService = groupService;
        }

        public ViewResult ShowStudents(int id)
        {
            ViewBag.Title = "Students";
            var group = _groupService.GetGroupByID(id);
            if(group != null)
            {
                ViewBag.CourseId = group.CourseId;
                var studetns = _studentService.GetStudentsByGroupID(id);
                ViewBag.GroupID = id;
                ViewBag.GroupName = group.Name;
                return View(studetns);
            }
            return View();
            
        }

        public ViewResult Edit(int id)
        {
            ViewBag.Title = "Edit Students";
            var student = _studentService.GetStudentByID(id);
            return View(student);
        }

        [HttpPost,ValidateAntiForgeryToken]

        public ActionResult Save(Student student)
        {
            bool result = _studentService.Save(student);
            if (result)
            {
                ViewBag.Message = "Successfully!";
            }
            else
            {
                ViewBag.Message = "Error,try again!";
            }
            return View(student);
        }

        public ActionResult Delete(int id)
        {
            var student = _studentService.GetStudentByID(id);
            bool result = _studentService.Delete(id);
            if (result)
            {
                ViewBag.Message = "Successfully!";
            }
            else
            {
                ViewBag.Message = "Error,try again!";
            }
            return View(student);
        }

        public ViewResult AddNewStudent(int groupId)
        {
            ViewBag.groupId = groupId;
            return View();
        }
    }
}
